Capgemini - shopping cart exercise
-----------------------
Technical Stack:
* Scala 2.12
* Gradle

Requirements:
* JDK8

Building the project:
* `./gradlew clean build`