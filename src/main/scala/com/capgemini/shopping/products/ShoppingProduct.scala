package com.capgemini.shopping.products

sealed abstract class ShoppingProduct(val price: BigDecimal)

case object APPLE extends ShoppingProduct(BigDecimal("0.6"))

case object ORANGE extends ShoppingProduct(BigDecimal("0.25"))

