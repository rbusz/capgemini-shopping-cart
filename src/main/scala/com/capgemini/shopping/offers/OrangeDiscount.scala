package com.capgemini.shopping.offers

import com.capgemini.shopping.products.{APPLE, ORANGE, ShoppingProduct}

/**
  * Calculates discount for a shopping basket
  */
trait Offer {
  def calculateDiscount(basket: List[ShoppingProduct]): BigDecimal
}

/**
  * Calculates discount 'buy one, get one free on Apples'
  */
class AppleDiscount extends Offer {
  override def calculateDiscount(basket: List[ShoppingProduct]): BigDecimal = {
    val applesCount = basket.count {
      case APPLE => true
      case _ => false
    }

    APPLE.price * BigDecimal(applesCount / 2)
  }
}

/**
  * Calculates discount '3 for the price of 2 on Oranges'
  */
class OrangeDiscount extends Offer {
  override def calculateDiscount(basket: List[ShoppingProduct]): BigDecimal = {
    val orangesCount = basket.count {
      case ORANGE => true
      case _ => false
    }

    ORANGE.price * BigDecimal(orangesCount / 3)
  }
}
