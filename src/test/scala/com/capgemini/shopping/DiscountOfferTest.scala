package com.capgemini.shopping

import com.capgemini.shopping.offers.{AppleDiscount, OrangeDiscount}
import com.capgemini.shopping.products.{APPLE, ORANGE}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSuite, Matchers}

@RunWith(classOf[JUnitRunner])
class DiscountOfferTest extends FunSuite with Matchers {

  test("Calculates 1 free apple discount for 'buy one, get one free'") {
    val offer = new AppleDiscount()
    offer.calculateDiscount(List(APPLE, ORANGE, APPLE)) should be (APPLE.price)
  }

  test("Calculates 0 discount for 'buy one, get one free'") {
    val offer = new AppleDiscount()
    offer.calculateDiscount(List(ORANGE, ORANGE, APPLE, ORANGE)) should be (0)
  }

  test("Calculates 2 free oranges discount for '3 of the price of 2'") {
    val offer = new OrangeDiscount()
    offer.calculateDiscount(List(APPLE, ORANGE, ORANGE, ORANGE, ORANGE, ORANGE, ORANGE)) should be (ORANGE.price * 2)
  }

  test("Calculates 0 free oranges discount for '3 of the price of 2'") {
    val offer = new OrangeDiscount()
    offer.calculateDiscount(List(ORANGE, ORANGE, APPLE))
  }

}
