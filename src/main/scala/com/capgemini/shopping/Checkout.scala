package com.capgemini.shopping

import com.capgemini.shopping.offers.Offer
import com.capgemini.shopping.products.ShoppingProduct

/**
  * Scans basket of products in order to calculate the price
  * @param offers - all available shopping offers that might discount the final price
  */
class Checkout(offers: Set[Offer]) {

  def this() = this(Set[Offer]())

  def scanProducts(basket: List[ShoppingProduct]): BigDecimal = {
    val productTotal = basket.foldLeft(BigDecimal(0))(_ + _.price)
    val discount = offers.foldLeft(BigDecimal(0))(_ + _.calculateDiscount(basket))

    BigDecimal(0).max(productTotal - discount)
  }
}
