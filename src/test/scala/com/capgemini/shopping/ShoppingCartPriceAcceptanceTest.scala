package com.capgemini.shopping

import com.capgemini.shopping.offers.Offer
import com.capgemini.shopping.products.{APPLE, ORANGE, ShoppingProduct}
import org.junit.runner.RunWith
import org.mockito
import org.mockito.BDDMockito.given
import org.mockito.Mockito.verify
import org.scalatest.junit.JUnitRunner
import org.scalatest.mockito.MockitoSugar.mock
import org.scalatest.{BeforeAndAfter, FunSuite, Matchers}

@RunWith(classOf[JUnitRunner])
class ShoppingCartPriceAcceptanceTest extends FunSuite with BeforeAndAfter with Matchers {

  test("Scan products then returns their total price") {
    val checkout = new Checkout()
    val actualPrice: BigDecimal = checkout.scanProducts(List[ShoppingProduct](APPLE, APPLE, ORANGE, APPLE))
    actualPrice should be (BigDecimal("2.05"))
  }
}

@RunWith(classOf[JUnitRunner])
class ShoppingCheckoutTest extends FunSuite with BeforeAndAfter with Matchers {

  var offerMock: Offer = _
  var checkout: Checkout = _

  before {
    offerMock = mock[Offer]
    checkout = new Checkout(Set(offerMock))
  }

  test("Checkout uses 20p discount offer to calculate price") {
    val discount = BigDecimal("0.2")
    val anyProductList = List(APPLE)

    given(offerMock.calculateDiscount(mockito.Matchers.any())).willReturn(discount)

    checkout.scanProducts(anyProductList) should be (0.4)

    verify(offerMock).calculateDiscount(mockito.Matchers.eq(anyProductList))
  }
}
